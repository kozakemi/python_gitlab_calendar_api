# -*- coding: UTF-8 -*-
import requests
import re
from http.server import BaseHTTPRequestHandler
import json

def list_split(items, n):
    return [items[i:i + n] for i in range(0, len(items), n)]
def getdata(name):
    try:
        gitpage = requests.get("https://gitlab.com/users/"+name+"/calendar.json")
        data = gitpage.text
        parsed_data = json.loads(data)
        total_contributions = sum(parsed_data.values())
        sorted_keys = sorted(parsed_data.keys())
        datalist = []
        for key in sorted_keys:
            itemlist = {"date": key, "count": parsed_data[key]}
            datalist.append(itemlist)
        datalistsplit = list_split(datalist, 7)
    except:
        total_contributions=0
        datalistsplit=[]
    returndata = {
        "total": total_contributions,
        "contributions": datalistsplit
    }
    return returndata
class handler(BaseHTTPRequestHandler):
    def do_GET(self):
        path = self.path
        user = path.split('?')[1]
        data = getdata(user)
        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(data).encode('utf-8'))
        return

print(getdata('kozakemi'))